﻿
Hosilter Backup (Mirror) on repo.or.cz
> This is a backup repo. The main repo is on github: https://github.com/rnrtralrzei/hosilter/

================================================================================================

    ▄█    █▄     ▄██████▄     ▄████████  ▄█   ▄█           ███        ▄████████    ▄████████
   ███    ███   ███    ███   ███    ███ ███  ███       ▀█████████▄   ███    ███   ███    ███
   ███    ███   ███    ███   ███    █▀  ███▌ ███          ▀███▀▀██   ███    █▀    ███    ███
  ▄███▄▄▄▄███▄▄ ███    ███   ███        ███▌ ███           ███   ▀  ▄███▄▄▄      ▄███▄▄▄▄██▀
 ▀▀███▀▀▀▀███▀  ███    ███ ▀███████████ ███▌ ███           ███     ▀▀███▀▀▀     ▀▀███▀▀▀▀▀  
   ███    ███   ███    ███          ███ ███  ███           ███       ███    █▄  ▀███████████
   ███    ███   ███    ███    ▄█    ███ ███  ███▌    ▄     ███       ███    ███   ███    ███
   ███    █▀     ▀██████▀   ▄████████▀  █▀   █████▄▄██    ▄████▀     ██████████   ███    ███
                                             ▀                                    ███    ███
											 
================================================================================================

Hosilter can block your children from entertainment websites. It makes the Internet healthy.

How to install it?
	Windows
		1.Download file.
		2.Cut the downloaded file to %SystemRoot%\System32\drivers\etc\.
		3.Restart all your browsers to take effect.
		4.Enjoy it and have lots of fun!
	Linux
		1.Run the following command:
			sudo wget -O /etc/hosts http://repo.or.cz/hosilter.git/blob_plain/HEAD:/basic/hosts
		2.Enjoy it and have lots of fun!


Copyright(C) Rnrtralrzei 2018. This repo uses GPL v2 License.

