@echo off
echo Making HOSTS...
echo ---------------Step 1:Make Temp Head File-----------------
copy head.txt head.tmp
echo --------------Step 2:Merge Domain Files-------------------
cd %cd%\domains\games
copy *.txt allgame.tmp
cd ../
echo .......................................................
cd %cd%\livestreaming
copy *.txt alllivestreaming.tmp
cd ../
cd ../
echo --------------Step 3:Move Merged Domain Files-----------
move %cd%\domains\games\allgame.tmp allgame.tmp
move %cd%\domains\livestreaming\alllivestreaming.tmp  alllivestreaming.tmp
echo -----------Step 4: Merge HOSTS File ------------------------
copy head.tmp+allgame.tmp+alllivestreaming.tmp hosts
del alllivestreaming.tmp
del allgame.tmp
del head.tmp
echo Done.
pause > NUL